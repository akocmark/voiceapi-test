<?php namespace Koodiph\Globelabsvoiceapi;

use Koodiph\Globelabsvoiceapi\Api;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class GlobelabsvoiceapiServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('koodiph/globelabsvoiceapi');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        App::bind('globelabs.voice.api', function($app)
        {
            return new Api\Tropo;
        });

        App::bind('globelabs.voice.api.result', function($app)
        {
            return new Api\Result;
        });

        App::bind('globelabs.voice.api.session', function($app)
        {
            return new Api\Session;
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
