<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* Event Helper class.
* @package TropoPHP_Support
*/
class Event {

  public static $continue = 'continue';
  public static $incomplete = 'incomplete';
  public static $error = 'error';
  public static $hangup = 'hangup';
  public static $join = 'join';
  public static $leave = 'leave';
  public static $ring = 'ring';
}

