<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* Channel Helper class.
* @package TropoPHP_Support
*/
class Channel {
  public static $voice = "VOICE";
  public static $text = "TEXT";
}
