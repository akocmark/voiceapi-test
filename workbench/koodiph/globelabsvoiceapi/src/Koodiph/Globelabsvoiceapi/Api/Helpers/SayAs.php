<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* SayAs Helper class.
* @package TropoPHP_Support
*/
class SayAs {
  public static $date = "DATE";
  public static $digits = "DIGITS";
  public static $number = "NUMBER";
}
