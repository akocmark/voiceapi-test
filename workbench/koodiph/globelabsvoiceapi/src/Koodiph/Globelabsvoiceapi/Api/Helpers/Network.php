<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* Network Helper class.
* @package TropoPHP_Support
*/
class Network {
  public static $pstn = "PSTN";
  public static $voip = "VOIP";
  public static $aim = "AIM";
  public static $gtalk = "GTALK";
  public static $jabber = "JABBER";
  public static $msn = "MSN";
  public static $sms = "SMS";
  public static $yahoo = "YAHOO";
  public static $twitter = "TWITTER";
}

