<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* Date Helper class.
* @package TropoPHP_Support
*/
class Date {
  public static $monthDayYear = "mdy";
  public static $dayMonthYear = "dmy";
  public static $yearMonthDay = "ymd";
  public static $yearMonth = "ym";
  public static $monthYear = "my";
  public static $monthDay = "md";
  public static $year = "y";
  public static $month = "m";
  public static $day = "d";
}
