<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* Voice Helper class.
* @package TropoPHP_Support
*/
class Voice {
  public static $Castilian_Spanish_male = "jorge";
  public static $Castilian_Spanish_female = "carmen";
  public static $French_male = "bernard";
  public static $French_female = "florence";
  public static $US_English_male = "dave";
  public static $US_English_female = "jill";
  public static $British_English_male = "dave";
  public static $British_English_female = "kate";
  public static $German_male = "stefan";
  public static $German_female = "katrin";
  public static $Italian_male = "luca";
  public static $Italian_female = "paola";
  public static $Dutch_male = "willem";
  public static $Dutch_female = "saskia";
  public static $Mexican_Spanish_male = "carlos";
  public static $Mexican_Spanish_female = "soledad";
}
