<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* Duration Helper class.
* @package TropoPHP_Support
*/
class Duration {
  public static $hoursMinutesSeconds = "hms";
  public static $hoursMinutes = "hm";
  public static $hours = "h";
  public static $minutes = "m";
  public static $seconds = "s";
}
