<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;


/**
* SIP Headers Helper class.
* @package TropoPHP_Support
*/
class Headers {

  public function __set($name, $value) {
    if(!strstr($name, "-")) {
      $this->$name = $value;
    } else {
      $name = str_replace("-", "_", $name);
      $this->$name = $value;
    }
  }
}
