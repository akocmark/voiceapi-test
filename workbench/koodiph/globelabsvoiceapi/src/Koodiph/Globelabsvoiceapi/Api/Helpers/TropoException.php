<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* A helper class for wrapping exceptions. Can be modified for custom excpetion handling.
*
*/
class TropoException extends \Exception { }
