<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* Format Helper class.
* @package TropoPHP_Support
*/
class Format {
  public $date;
  public $duration;
  public static $ordinal = "ordinal";
  public static $digits = "digits";

  public function __construct($date=NULL, $duration=NULL) {
    $this->date = $date;
    $this->duration = $duration;
  }
}

