<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* Recognizer Helper class
* @package TropoPHP_Support
*
*/
class Recognizer {
  public static $German = 'de-de';
  public static $British_English = 'en-gb';
  public static $US_English = 'en-us';
  public static $Castilian_Spanish = 'es-es';
  public static $Mexican_Spanish = 'es-mx';
  public static $French_Canadian = 'fr-ca';
  public static $French = 'fr-fr';
  public static $Italian = 'it-it';
  public static $Polish = 'pl-pl';
  public static $Dutch = 'nl-nl';
}
