<?php namespace Koodiph\Globelabsvoiceapi\Api\Helpers;

/**
* AudioFormat Helper class.
* @package TropoPHP_Support
*/
class AudioFormat {
  public static $wav = "audio/wav";
  public static $mp3 = "audio/mp3";
}
