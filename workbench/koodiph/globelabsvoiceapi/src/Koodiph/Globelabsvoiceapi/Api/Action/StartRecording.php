<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\BaseClass;

/**
* Allows Tropo applications to begin recording the current session.
* The resulting recording may then be sent via FTP or an HTTP POST/Multipart Form.
* @package TropoPHP_Support
*
*/
class StartRecording extends BaseClass {

  private $_name;
  private $_format;
  private $_method;
  private $_password;
  private $_url;
  private $_username;
  private $_transcriptionID;
  private $_transcriptionEmailFormat;
  private $_transcriptionOutURI;

  /**
  * Class constructor
  *
  * @param string $name
  * @param string $format
  * @param string $method
  * @param string $password
  * @param string $url
  * @param string $username
  * @param string $transcriptionID
  * @param string $transcriptionEmailFormat
  * @param string $transcriptionOutURI
  */
  public function __construct($format=NULL, $method=NULL, $password=NULL, $url=NULL, $username=NULL, $transcriptionID=NULL, $transcriptionEmailFormat=NULL, $transcriptionOutURI=NULL) {
    $this->_format = $format;
    $this->_method = $method;
    $this->_password = $password;
    $this->_url = $url;
    $this->_username = $username;
    $this->_transcriptionID = $transcriptionID;
    $this->_transcriptionEmailFormat = $transcriptionEmailFormat;
    $this->_transcriptionOutURI = $transcriptionOutURI;
  }

  /**
  * Renders object in JSON format.
  *
  */
  public function __toString() {
    if(isset($this->_format)) { $this->format = $this->_format; }
    if(isset($this->_method)) { $this->method = $this->_method; }
    if(isset($this->_password)) { $this->password = $this->_password; }
    if(isset($this->_url)) { $this->url = $this->_url; }
    if(isset($this->_username)) { $this->username = $this->_username; }
    if(isset($this->_transcriptionID)) { $this->transcriptionID = $this->_transcriptionID; }
    if(isset($this->_transcriptionEmailFormat)) { $this->transcriptionEmailFormat = $this->_transcriptionEmailFormat; }
    if(isset($this->_transcriptionOutURI)) { $this->transcriptionOutURI = $this->_transcriptionOutURI; }
    return $this->unescapeJSON(json_encode($this));
  }
}
