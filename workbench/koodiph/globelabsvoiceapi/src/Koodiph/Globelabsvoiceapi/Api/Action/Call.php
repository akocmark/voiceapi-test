<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\BaseClass;

/**
* This object allows Tropo to make an outbound call. The call can be over voice or one
* of the text channels.
* @package TropoPHP_Support
*
*/
class Call extends BaseClass {

  private $_to;
  private $_from;
  private $_network;
  private $_channel;
  private $_answerOnMedia;
  private $_timeout;
  private $_headers;
  private $_recording;
  private $_allowSignals;
  private $_machineDetection;
  private $_voice;

  /**
  * Class constructor
  *
  * @param string $to
  * @param string $from
  * @param string $network
  * @param string $channel
  * @param boolean $answerOnMedia
  * @param int $timeout
  * @param array $headers
  * @param StartRecording $recording
  * @param string|array $allowSignals
  */
  public function __construct($to, $from=NULL, $network=NULL, $channel=NULL, $answerOnMedia=NULL, $timeout=NULL, Array $headers=NULL, StartRecording $recording=NULL, $allowSignals=NULL, $machineDetection=NULL, $voice=NULL) {
    $this->_to = $to;
    $this->_from = $from;
    $this->_network = $network;
    $this->_channel = $channel;
    $this->_answerOnMedia = $answerOnMedia;
    $this->_timeout = $timeout;
    $this->_headers = $headers;
    $this->_recording = isset($recording) ? sprintf('%s', $recording) : null ;
    $this->_allowSignals = $allowSignals;
    $this->_machineDetection = $machineDetection;
    $this->_voice = $voice;
  }

  /**
  * Renders object in JSON format.
  *
  */
  public function __toString() {
    $this->to = $this->_to;
    if(isset($this->_from)) { $this->from = $this->_from; }
    if(isset($this->_network)) { $this->network = $this->_network; }
    if(isset($this->_channel)) { $this->channel = $this->_channel; }
    if(isset($this->_timeout)) { $this->timeout = $this->_timeout; }
    if(isset($this->_answerOnMedia)) { $this->answerOnMedia = $this->_answerOnMedia; }
    if(count($this->_headers)) { $this->headers = $this->_headers; }
    if(isset($this->_recording)) { $this->recording = $this->_recording; }
    if(isset($this->_allowSignals)) { $this->allowSignals = $this->_allowSignals; }
    if(isset($this->_machineDetection)) {
      if(is_bool($this->_machineDetection)){
        $this->machineDetection = $this->_machineDetection; 
      }else{
        $this->machineDetection->introduction = $this->_machineDetection; 
        if(isset($this->_voice)){
          $this->machineDetection->voice = $this->_voice; 
        }
      }
    }
    return $this->unescapeJSON(json_encode($this));
  }
}

