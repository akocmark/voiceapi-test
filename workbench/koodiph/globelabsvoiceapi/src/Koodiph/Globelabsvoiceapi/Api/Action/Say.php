<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\BaseClass;

/**
* When the current session is a voice channel this key will either play a message or an audio file from a URL.
* In the case of an text channel it will send the text back to the user via instant messaging or SMS.
* @package TropoPHP_Support
*
*/
class Say extends BaseClass {

  private $_value;
  private $_as;
  private $_event;
  private $_format;
  private $_voice;
  private $_allowSignals;

  /**
  * Class constructor
  *
  * @param string $value
  * @param SayAs $as
  * @param string $event
  * @param string $voice
  * @param string|array $allowSignals
  */
  public function __construct($value, $as=NULL, $event=NULL, $voice=NULL, $allowSignals=NULL) {
    $this->_value = $value;
    $this->_as = $as;
    $this->_event = $event;
    $this->_voice = $voice;
    $this->_allowSignals = $allowSignals;
  }

  /**
  * Renders object in JSON format.
  *
  */
  public function __toString() {
    if(isset($this->_event)) { $this->event = $this->_event; }
    $this->value = $this->_value;
    if(isset($this->_as)) { $this->as = $this->_as; }
    if(isset($this->_voice)) { $this->voice = $this->_voice; }
    if(isset($this->_allowSignals)) { $this->allowSignals = $this->_allowSignals; }
    return $this->unescapeJSON(json_encode($this));
  }
}
