<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\BaseClass;

/**
* Defnies an endoint for transfer and redirects.
* @package TropoPHP_Support
*
*/
class Endpoint extends BaseClass {

  private $_id;
  private $_channel;
  private $_name = 'unknown';
  private $_network;

  /**
  * Class constructor
  *
  * @param string $id
  * @param string $channel
  * @param string $name
  * @param string $network
  */
  public function __construct($id, $channel=NULL, $name=NULL, $network=NULL) {

    $this->_id = $id;
    $this->_channel = $channel;
    $this->_name = $name;
    $this->_network = $network;
  }

  /**
  * Renders object in JSON format.
  *
  */
  public function __toString() {

    if(isset($this->_id)) { $this->id = $this->_id; }
    if(isset($this->_channel)) { $this->channel = $this->_channel; }
    if(isset($this->_name)) { $this->name = $this->_name; }
    if(isset($this->_network)) { $this->network = $this->_network; }
    return $this->unescapeJSON(json_encode($this));
  }
}
