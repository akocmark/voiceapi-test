<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

/**
* Base class for empty actions.
* @package TropoPHP_Support
*
*/
class EmptyBaseClass {

  final public function __toString() {
    return json_encode(null);
  }
}

