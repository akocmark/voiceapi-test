<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\EmptyBaseClass;

/**
* Stop an already started recording.
* @package TropoPHP_Support
*
*/
class StopRecording extends EmptyBaseClass { }

