<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\BaseClass;

/**
* Transcribes spoken text.
* @package TropoPHP_Support
*
*/
class Transcription extends BaseClass {

  private $_url;
  private $_id;
  private $_emailFormat;

  /**
  * Class constructor
  *
  * @param string $url
  * @param string $id
  * @param string $emailFormat
  */
  public function __construct($url, $id=NULL, $emailFormat=NULL) {
    $this->_url = $url;
    $this->_id = $id;
    $this->_emailFormat = $emailFormat;
  }

  /**
  * Renders object in JSON format.
  *
  */
  public function __toString() {
    if(isset($this->_id)) { $this->id = $this->_id; }
    if(isset($this->_url)) { $this->url = $this->_url; }
    if(isset($this->_emailFormat)) { $this->emailFormat = $this->_emailFormat; }
    return $this->unescapeJSON(json_encode($this));
  }
}
