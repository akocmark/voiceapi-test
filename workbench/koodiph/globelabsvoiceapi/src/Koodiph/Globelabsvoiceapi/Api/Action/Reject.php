<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\EmptyBaseClass;

/**
* Allows Tropo applications to reject incoming sessions before they are answered.
* @package TropoPHP_Support
*
*/
class Reject extends EmptyBaseClass { }
