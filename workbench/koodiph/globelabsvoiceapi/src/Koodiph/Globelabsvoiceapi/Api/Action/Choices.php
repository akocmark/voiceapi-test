<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\BaseClass;

/**
* Defines the input to be collected from the user.
* @package TropoPHP_Support
*/
class Choices extends BaseClass {

  private $_value;
  private $_mode;
  private $_terminator;

  /**
  * Class constructor
  *
  * @param string $value
  * @param string $mode
  * @param string $terminator
  */
  public function __construct($value=NULL, $mode=NULL, $terminator=NULL) {
    $this->_value = $value;
    $this->_mode = $mode;
    $this->_terminator = $terminator;
  }

  /**
  * Renders object in JSON format.
  *
  */
  public function __toString() {
    if(isset($this->_value)){ $this->value = $this->_value; }
    if(isset($this->_mode)) { $this->mode = $this->_mode; }
    if(isset($this->_terminator)) { $this->terminator = $this->_terminator; }
    return $this->unescapeJSON(json_encode($this));
  }
}
