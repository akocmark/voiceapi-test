<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

/**
* Base class for Tropo class and indvidual Tropo action classes.
* Derived classes must implement both a constructor and __toString() function.
* @package TropoPHP_Support
* @abstract BaseClass
*/

abstract class BaseClass {

  /**
  * toString Function
  * @abstract __toString()
  */
  abstract public function __toString();

  /**
  * Allows derived classes to set Undeclared properties.
  *
  * @param mixed $attribute
  * @param mixed $value
  */
  public function __set($attribute, $value) {
    $this->$attribute= $value;
  }

  /**
  * Removes escape characters from a JSON string.
  *
  * @param string $json
  * @return string
  */
  public function unescapeJSON($json) {
    return str_replace(array('\"', "\"{", "}\"", '\\\\\/', '\\\\'), array('"', "{", "}", '/', '\\'), $json);
  }
}
