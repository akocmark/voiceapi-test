<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\EmptyBaseClass;

/**
* This function instructs Tropo to "hang-up" or disconnect the session associated with the current session.
* @package TropoPHP_Support
*
*/
class Hangup extends EmptyBaseClass { }
