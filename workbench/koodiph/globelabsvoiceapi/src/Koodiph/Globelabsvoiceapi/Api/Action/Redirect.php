<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\BaseClass;

/**
* The redirect function forwards an incoming call to another destination / phone number before answering it.
* @package TropoPHP_Support
*
*/
class Redirect extends BaseClass {

  private $_to;
  private $_from;

  /**
  * Class constructor
  *
  * @param Endpoint $to
  * @param Endpoint $from
  */
  public function __construct($to=NULL, $from=NULL) {
    $this->_to = sprintf('%s', $to);
    $this->_from = isset($from) ? sprintf('%s', $from) : null;
  }

  /**
  * Renders object in JSON format.
  *
  */
  public function __toString() {
    $this->to = $this->_to;
    if(isset($this->_from)) { $this->from = $this->_from; }
    return $this->unescapeJSON(json_encode($this));
  }
}
