<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\BaseClass;

/**
* Adds an event callback so that your application may be notified when a particular event occurs.
* @package TropoPHP_Support
*
*/
class On extends BaseClass {

  private $_event;
  private $_next;
  private $_say;
  private $_voice;
  private $_ask;
  private $_message;
  private $_wait;
  private $_order;

  /**
  * Class constructor
  *
  * @param string $event
  * @param string $next
  * @param Say $say
  * @param string $voice
  */
  public function __construct($event=NULL, $next=NULL, Say $say=NULL, $voice=Null, $ask=NULL, Message $message=NULL, Wait $wait=NULL, $order=NULL) {
    $this->_event = $event;
    $this->_next = $next;
    $this->_say = isset($say) ? sprintf('%s', $say) : null ;
    $this->_voice = $voice;
    $this->_ask = isset($ask) ? sprintf('%s', $ask) : null;
    $this->_message = isset($message) ? sprintf('%s', $message) : null;
    $this->_wait = isset($wait) ? sprintf('%s', $wait) : null;
    $this->_order = $order;
  }

  /**
  * Renders object in JSON format.
  *
  */
  public function __toString() {
    
    if($this->_event == "connect") {  
      $this->event =  $this->_event;        
      switch($this->_order){
        case 'ask':
        $this->ask = $this->_ask;
        break;
        case  'say':
        $this->say = $this->_say;
        break;
        case 'wait':
        $this->ask = $this->_ask;  
        break;
        case 'message':
        $this->message = $this->_message;
        break;
      }          
      return $this->unescapeJSON(json_encode(($this)));
    }else{
      if(isset($this->_event)) { $this->event = $this->_event; }
      if(isset($this->_next)) { $this->next = $this->_next; }
      if(isset($this->_say)) { $this->say = $this->_say; }
      if(isset($this->_voice)) { $this->voice = $this->_voice; }
      return $this->unescapeJSON(json_encode($this));
    }
  }
}

