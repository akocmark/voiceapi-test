<?php namespace Koodiph\Globelabsvoiceapi\Api\Action;

use Koodiph\Globelabsvoiceapi\Api\Action\BaseClass;

/**
* Defines a time period to sleep in milliseconds
* @package TropoPHP_Support
*
*/
class Wait extends BaseClass {

  private $_milliseconds;
  private $_allowSignals;

  /**
  * Class constructor
  *
  * @param integer $milliseconds
  * @param string|array $allowSignals
  */
  public function __construct($milliseconds, $allowSignals=NULL) {
    $this->_milliseconds = $milliseconds;
    $this->_allowSignals = $allowSignals;
  }

  /**
  * Renders object in JSON format.
  *
  */
  public function __toString() {
    $this->milliseconds = $this->_milliseconds; 
    if(isset($this->_allowSignals)) { $this->allowSignals = $this->_allowSignals; }
    return $this->unescapeJSON(json_encode($this));
  }
}
