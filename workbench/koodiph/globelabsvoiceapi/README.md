### Setting up ###

- require this package in your composer.json
- run ` $ composer update `
- add this package's Service Provider in your app's config ` 'Koodiph\Globelabsvoiceapi\GlobelabsvoiceapiServiceProvider' `

#### Answering Calls ####

```
#!php
$tropo = App::make('globelabs.voice.api');
$tropo->say("Welcome to GlobeLabs Voice API!");
$tropo->RenderJson();
```
Answer is handled automatically for you, and hangup is optional as the call will disconnect when the script ends.

#### Making Calls ####

```
#!php
$tropo = App::make('globelabs.voice.api');
$tropo->call("+14155550100");
$tropo->say("Tag, you're it!");

$tropo->RenderJson();
```
*However Tropo says in order to make outbound calls or send outbound SMS at all, you will also need to verify your account. Please open an account ticket to begin the process.*

#### Sending SMS ####

```
#!php
$tropo = App::make('globelabs.voice.api');
$tropo->call("+14155550100", array('network'=>'SMS'));
$tropo->say("Tag, you're it!");
$tropo->RenderJson();
```

#### Asking a Question ####

```
#!php
Route::post('gcash', function()
{
    $options = array("choices" => "[1 DIGIT]", "name" => "digit", "attempts" => 3);

    $tropo = App::make('globelabs.voice.api');    
    $tropo->ask("Hi there, welcome to globe voice test. Please choose a number from zero to nine", $options);

    $tropo->on(array("event" => "continue", "next" => "gcash/continue"));
    $tropo->on(array("event" => "hangup", "next" => "gcash/hang"));
    $tropo->on(array("event" => "error", "next" => "gcash/error"));
    $tropo->on(array("event" => "incomplete", "next" => "gcash/incomplete"));

    $tropo->RenderJson();
});

Route::post('gcash/continue', function()
{
    $tropo     = App::make('globelabs.voice.api');
    @$result = App::make('globelabs.voice.api.result');
    $answer  = $result->getValue();

    $tropo->say('You choose ' . $answer . '. Goodbye!');
    $tropo->RenderJson();
});
```

#### Recording the Call ####

```
#!php
Route::post('gcash/recording', function()
{
    $tropo = App::make('globelabs.voice.api');
    $tropo->startRecording(array("url" => $url . "/recordings"));
    $tropo->say("Hi! this call is being recorded.");
    $tropo->stopRecording();

    $tropo->RenderJson();
});

Route::post('recordings', function()
{
    Log::info('recording', Input::all());
});
```

#### Rejecting a Call ####

```
#!php
$tropo     = App::make('globelabs.voice.api');
$session = App::make('globelabs.voice.api.session');

$from      = $session->getFrom();
$callerID = $from["id"];

if ($callerID == '4075550100' || $callerID == '9165550100') {
    $tropo->reject();
} else {
    $tropo->say("Hi friend!");
}

$tropo->RenderJson();
```

#### Transferring a Call ####

```
#!php
$tropo = App::make('globelabs.voice.api');

$tropo->say("Transferring you now, please wait");
$tropo->transfer("+14075550100");

$tropo->RenderJson();
```

#### Creating a Conference Call ####

```
#!php
$tropo = App::make('globelabs.voice.api');

$tropo->say("Transferring you now, please wait");
$tropo->conference(null, array("name"=>"conference", "id" => "1234"));

$tropo->RenderJson();
```

Check the official Tropo WebAPI Reference
[WebAPI Reference](https://www.tropo.com/docs/webapi/reference)



