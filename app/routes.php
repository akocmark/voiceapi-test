<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::post('gcash', array('after' => 'asd', function()
{
	// $tropo = new Tropo();    

	// // Use Tropo's text to speech to say a phrase.    
	// $tropo->say('Yes, Tropo is this easy.');
	// $tropo->renderJSON();

	$tropo = App::make('globelabs.voice.api');    

    $tropo->startRecording(array("url" => "http://mark.koodidojo.com/recording"));

    $options = array("choices" => "[1 DIGIT]", "name" => "digit", "attempts" => 3);

    $tropo->ask("Hi there, welcome to globe voice test. Please choose a number from zero to nine", $options);

    $tropo->on(array("event" => "continue", "next" => "asd"));

    $tropo->on(array("event" => "hangup", "next" => "hang"));

    $tropo->on(array("event" => "error", "next" => "err"));

    $tropo->on(array("event" => "incomplete", "next" => "inc"));
    
    $tropo->stopRecording();

    $tropo->RenderJson();

}));

Route::match(array('GET', 'POST', 'PUT'), 'recording', function() {
    if (Input::hasFile('filename')) {
        Log::info('shitness', (array) Input::file('filename'));
    }
});

Route::post('inc', array('after' => 'asd', function()
{
    $tropo = App::make('globelabs.voice.api');   

    $tropo->say("Sorry, that wasn't one of the options.");

    $tropo->RenderJson();

}));

Route::post('asd', array('after' => 'asd', function()
{
    $tropo = App::make('globelabs.voice.api');

    @$result = App::make('globelabs.voice.api.result');

    $answer = $result->getValue();

    $tropo->say('You choose ' . $answer . '. Goodbye!');

    $tropo->RenderJson();

}));

Route::post('hang', array('after' => 'asd', function()
{
    // log
}));

Route::post('err', array('after' => 'asd', function()
{
    // log
}));